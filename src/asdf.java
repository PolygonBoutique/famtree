import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class asdf {

    //////////////////////////////////////////////////////////////////////////////
    // Daten
    //////////////////////////////////////////////////////////////////////////////


    static final String MARIA = "Maria";
    static final String FLORIAN = "Florian";
    static final String DAVID = "David";
    static final String ANNE = "Anne";
    static final String JAKOB = "Jakob";
    static final String TANJA = "Tanja";
    static final String ADAM = "Adam";
    static final String ANKE = "Anke";
    static final String GUSTAV = "Gustav";
    static final String KLAUS = "Klaus";
    static final String SUSANNE = "Susanne";
    static final String BERND = "Bernd";
    static final String DORIS = "Doris";
    static final String EVA = "Eva";
    static final String KAI = "Kai";
    static final String BIRGIT = "Birgit";
    static final String RALF = "Ralf";
    static final String NINA = "Nina";
    static final String TIM = "Tim";
    static final String SOPHIE = "Sophie";
    static final String MIA = "Mia";
    static final String PIA = "Pia";
    static final String MORITZ = "Moritz";
    static final String FINN = "Finn";
    static final String MAX = "Max";
    static final String SARAH = "Sarah";
    static final String LEONIE = "Leonie";
    static final String FABIAN = "Fabian";
    ////////////////////
    static final String LEA = "Lea";
    static final String BIBI = "Bibi";
    static final String BOBO = "Bobo";
    static final String TUTI = "Tuti";
    static final String BOB = "Bob";
    static final String BERG = "Berg";
    static final String MARTA = "Marta";
    static final String TOMAS = "Tomas";

    static final String STANLEY = "Stanley";
    static final String SALLY = "Sally";
    static final String OLGA = "Olga";
    static final String EVGENY = "Evgeny";
    static final String IRINA = "Irina";
    static final String WALDEMAR = "Waldemar";
    static final String ALEXANDER = "Alexander";
    static final String ROSA = "Rosa";
    static final String ANDREY = "Andrey";
    static final String VIKTOR = "Viktor";
    static final String VALENTINA = "Valentina";
    static final String LILIA = "Lilia";


    //                                           Mutter,  Vater
    static final String[] s_Abstammung = {DAVID, MARIA, FLORIAN,
            TANJA, ANNE, JAKOB,
            ADAM, TANJA, DAVID,
            KLAUS, ANKE, GUSTAV,
            DORIS, SUSANNE, BERND,
            EVA, DORIS, KLAUS,
            KAI, EVA, ADAM,
            BIRGIT, MARIA, FLORIAN,
            RALF, MARIA, FLORIAN,
            NINA, ANNE, JAKOB,
            TIM, ANNE, JAKOB,
            SOPHIE, NINA, RALF,
            MIA, ANKE, BERND,
            PIA, ANKE, BERND,
            MORITZ, SUSANNE, GUSTAV,
            FINN, SUSANNE, GUSTAV,
            MAX, PIA, FINN,
            SARAH, SOPHIE, MAX,
            GUSTAV, LEONIE, FABIAN,

            LEONIE, LEA, BOB,
            FABIAN, MARTA, TOMAS,
            LEA, BIBI, BOBO,
            BOB, TUTI, BERG,

            BERG,OLGA,EVGENY,
            OLGA,IRINA,WALDEMAR,
            EVGENY,VALENTINA,VIKTOR,
            VALENTINA,LILIA,ANDREY,
            VIKTOR,ROSA,ALEXANDER};

    //////////////////////////////////////////////////////////////////////////////
    // main
    //////////////////////////////////////////////////////////////////////////////

    private static final Map<String, String[]> m_Abstammung = new HashMap<>();
    private static final StringBuilder output = new StringBuilder();
    private static final String ROW = " -+-";
    private static final String ROW_END = " -+ ";
    private static final String COLUMN = "  ! ";

    public static void main(String[] args) {
        long start = System.nanoTime();

        initDatabase();

        var name = "Sarah";
        output.append(name);
        output.append(",");

        motherMainTree(name);
        fatherMainTree(name);

        List<String[]> format = formatOutput();

        for (String[] strings : format) {
            for (String string : strings) {
                System.out.print(string + " ");
            }
            System.out.println();
        }

        long end = System.nanoTime();
        long elapsed = end - start;
        double seconds =  elapsed / 1_000_000_000.0;
        System.out.printf("EXEC TIME : %f seconds\n", seconds);
    }

    private static void initDatabase() {
        for (var i = 0; i < s_Abstammung.length; i++) {
            if ((i + 1) % 3 == 0) {
                String child = s_Abstammung[i - 2];
                String mother = s_Abstammung[i - 1];
                String father = s_Abstammung[i];
                String[] parents = new String[]{mother, father};
                m_Abstammung.put(child, parents);
            }
        }
    }

    private static void motherMainTree(String name) {
        for (Entry<String, String[]> entry : m_Abstammung.entrySet()) {
            if (entry.getKey().equals(name)) {
                name = entry.getValue()[0];
                output.append(name);
                output.append(",");
                motherMainTree(name);
                break;
            }
        }
        subTree(name);
    }

    private static void subTree(String name) {
        for (Entry<String, String[]> entry : m_Abstammung.entrySet()) {
            if (entry.getKey().equals(name)) {
                name = entry.getValue()[1];
                output.append(name);
                output.append(",");
                motherMainTree(name);
                subTree(name);
                break;
            }
        }
        output.append("\n");
    }


    private static void fatherMainTree(String name) {
        for (Entry<String, String[]> entry : m_Abstammung.entrySet()) {
            if (entry.getKey().equals(name)) {
                name = entry.getValue()[1];
                output.append(name);
                output.append(",");
                motherMainTree(name);
                break;
            }
        }
        subTree(name);
    }

    private static List<String[]> splitOutputString() {
        // Split input string by '\n'
        String[] split = output.toString().split("\n");

        List<String[]> listOfArrays = new ArrayList<>();

        // Split string array by ','
        for (String s : split) {
            // Scip empty lines
            if (!s.equals("")) {
                String[] temp = s.split(",");

                // Reverse string array
                temp = reverseStringArray(temp);
                listOfArrays.add(temp);
            }
        }
        return listOfArrays;
    }

    private static String[] reverseStringArray(String[] s) {
        List<String> list = Arrays.asList(s);
        Collections.reverse(list);
        s = list.toArray(new String[0]);
        return s;
    }

    private static List<String[]> addLeadingSpace(List<String[]> list) {
        List<String[]> modifiedOutput = new LinkedList<>();
        List<String> tempList = new LinkedList<>();

        for (Entry<Integer, Integer> entry : getLookup(list).entrySet()) {
            var count = 0;
            while (count < entry.getValue()) {
                tempList.add(getEmptyString());
                count++;
            }

            Collections.addAll(tempList, list.get(entry.getKey()));

            // Convert tempList to tempString
            String[] tempString = tempList.toArray(String[]::new);

            modifiedOutput.add(tempString);
            tempList.clear();
        }

        return modifiedOutput;
    }

    private static Map<Integer, Integer> editLookup(Map<Integer, Integer> map) {
        var maxValue = 0;

        for (Integer value : map.values()) {
            if (value > maxValue) {
                maxValue = value;
            }
        }
        for (Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 0) {
                map.put(entry.getKey(), maxValue);
            } else if (entry.getValue() == maxValue) {
                map.put(entry.getKey(), 0);
            } else {
                map.put(entry.getKey(), maxValue - entry.getValue());
            }
        }

        return map;
    }

    private static Map<Integer, Integer> getLookup(List<String[]> list) {
        Map<Integer, Integer> lookup = new HashMap<>();

        var line1 = 0;
        var line2 = 0;
        var count = 0;
        var move = 0;
        var scipLines = 0;

        for (var i = 0; i < list.size(); i++) {
            String[] currStrArray = list.get(i);
            count++;

            for (var j = 0; j < currStrArray.length; j++) {

                if (scipLines > 0) {
                    scipLines--;
                    count = 0;
                    break;
                }

                if (count == 1) {
                    line1 = currStrArray.length;
                } else {
                    line2 = currStrArray.length;
                    count = 0;

                    lookup.put(i - 1, 0);
                    if (line1 > line2) {
                        lookup.put(i, 0);
                    } else {
                        move = line2 - 1;
                        scipLines = move * 2;

                        var counter = 0;
                        var pair = 0;

                        while (counter < scipLines) {
                            pair++;
                            if (pair != 2) {
                                if (counter >= 2) {
                                    var value = list.get(i + counter).length - 1 + move;
                                    lookup.put(i + counter, value);
                                    lookup.put(i + 1 + counter, value);
                                } else {
                                    lookup.put(i + counter, move);
                                    lookup.put(i + 1 + counter, move);
                                }
                            }

                            if (pair == 2) {
                                pair = 0;
                                move--;
                            }
                            counter++;
                        }
                    }
                }
            }
        }
        return editLookup(lookup);
    }

    private static List<String[]> formatOutput() {
        List<String[]> split = splitOutputString();
        List<String[]> length = editStringLength(split);
        List<String[]> rows = addRows(length);
        List<String[]> leadingSpace = addLeadingSpace(rows);
        List<String[]> trailingSpace = addTrailingSpace(leadingSpace);
        List<String[]> columns = addColumns(trailingSpace);
        List<String[]> whiteSpaces = removeWhiteSpace(columns);

        return whiteSpaces;
    }

    private static List<String[]> addRows(List<String[]> list) {
        List<String[]> modifiedOutput = new ArrayList<>();
        List<String> tempList = new LinkedList<>();

        for (var i = 0; i < list.size(); i++) {
            String[] s = list.get(i);
            for (var j = 0; j < s.length; j++) {
                if (j + 1 <= s.length) {
                    if (j < s.length - 1) {
                        tempList.add(s[j] + ROW);
                    } else if (i > 0) {
                        tempList.add(s[j] + ROW_END);
                    } else {
                        tempList.add(s[j]);
                    }
                }
            }

            // Convert tempList to tempString
            String[] tempString = tempList.toArray(String[]::new);

            modifiedOutput.add(tempString);
            tempList.clear();
        }
        return modifiedOutput;
    }

    private static List<String[]> addTrailingSpace(List<String[]> list) {
        List<String[]> modifiedOutput = new LinkedList<>();
        List<String> tempList = new LinkedList<>();

        String[] previousLine = new String[0];

        for (var i = 0; i < list.size(); i++) {
            if (i > 0) {
                var deference = previousLine.length - list.get(i).length;
                Collections.addAll(tempList, list.get(i));
                while (deference > 0) {
                    deference--;
                    tempList.add(getEmptyString());
                }
            } else {
                Collections.addAll(tempList, list.get(i));
            }

            // Convert tempList to tempString
            String[] tempString = tempList.toArray(String[]::new);

            previousLine = tempString;

            modifiedOutput.add(tempString);
            tempList.clear();
        }

        return modifiedOutput;
    }

    private static List<String[]> addColumns(List<String[]> list) {
        List<String[]> modifiedOutput = new LinkedList<>();
        List<String> tempList = new LinkedList<>();

        String[] previousLine = new String[0];

        for (var i = 0; i < list.size(); i++) {
            if (i > 0) {
                for (var j = 0; j < list.get(i).length; j++) {
                    if ((previousLine[j].endsWith(ROW) || previousLine[j].endsWith(COLUMN)) && list
                            .get(i)[j].endsWith(getEmptyString())) {
                        tempList.add(getEmptyString().substring(0, getEmptyString().length() - ROW.length())
                                + COLUMN); // Replace the last part of 'getString()' (" -+-") by "  ! "
                    } else {
                        tempList.add(list.get(i)[j]);
                    }
                }
            } else {
                Collections.addAll(tempList, list.get(i));
            }

            // Convert tempList to tempString
            String[] tempString = tempList.toArray(String[]::new);

            previousLine = tempString;

            modifiedOutput.add(tempString);
            tempList.clear();
        }

        return separateRows(modifiedOutput);


    }

    private static List<String[]> separateRows(List<String[]> list) {
        List<String[]> modifiedOutput = new LinkedList<>();
        List<String> tempList = new LinkedList<>();

        for (var i = 0; i < list.size(); i++) {

            modifiedOutput.add(list.get(i));

            if (!getRegex(list.get(i))) {
                for (var j = 0; j < list.get(i).length; j++) {
                    if (list.get(i)[j].endsWith(COLUMN)) {
                        tempList.add(getEmptyString().substring(0, getEmptyString().length() - ROW.length())
                                + COLUMN); // Replace the last part of 'getString()' (" -+-") by "  ! "
                    } else {
                        tempList.add(getEmptyString());
                    }
                }

                // Convert tempList to tempString
                String[] tempString = tempList.toArray(String[]::new);

                if (i < list.size() - 1) {
                    modifiedOutput.add(tempString);
                }
                tempList.clear();
            }
        }

        return modifiedOutput;
    }

    private static boolean getRegex(String[] strings) {
        for (String string : strings) {
            if (string.endsWith(ROW)) {
                return true;
            }
        }
        return false;
    }

    private static List<String[]> editStringLength(List<String[]> list) {
        List<String[]> modifiedOutput = new ArrayList<>();
        List<String> tempList = new LinkedList<>();
        int size = getSizeOfLongestString();

        for (String[] strings : list) {
            for (String s : strings) {
                while (s.length() < size) {
                    s = s + " ";
                }
                tempList.add(s);
            }

            // Convert tempList to tempString
            String[] tempString = tempList.toArray(String[]::new);

            modifiedOutput.add(tempString);
            tempList.clear();
        }
        return modifiedOutput;
    }

    private static List<Integer> getStringLength(List<String[]> list) {

        var size = list.size();
        var count = 0;
        List<Integer> listOfStringLengths = new LinkedList<>();

        while (size > count) {
            var length = 0;

            for (String[] strings : list) {
                for (var j = 0; j < strings.length; j++) {
                    if (j == count) {
                        if (strings[j].length() > length) {
                            length = strings[j].length();
                            break;
                        }
                    }
                }
            }
            if (length > 0) {
                listOfStringLengths.add(length);
            }
            count++;
        }
        return listOfStringLengths;
    }

    private static int getSizeOfLongestString() {
        var size = 0;
        for (String[] strings : splitOutputString()) {
            for (String s : strings) {
                if (s.length() > size) {
                    size = s.length();
                }
            }
        }
        return size;
    }

    private static String getEmptyString() {
        var sb = new StringBuilder();
        while (sb.length() < getSizeOfLongestString()) {
            sb.append(" ");
        }
        sb.append("    "); // Add extra empty lines for " -+-"
        return sb.toString();
    }

    private static List<String[]> removeWhiteSpace(List<String[]> list) {
        List<String[]> modifiedOutput = new ArrayList<>();
        List<String> tempList = new LinkedList<>();
        List<Integer> whiteSpaces = getListOfWhiteSpaces(list);

        for (String[] strings : list) {
            for (var j = 0; j < strings.length; j++) {
                var num = whiteSpaces.get(j) - 1;
                var sb = new StringBuilder(strings[j]);
                while (num > 0) {
                    for (var k = 0; k < sb.length(); k++) {
                        if (Character.isSpaceChar(sb.charAt(k))) {
                            sb.replace(k, k + 1, "");
                            break;
                        }
                    }
                    num--;
                }
                tempList.add(sb.toString());
            }

            // Convert tempList to tempString
            String[] tempString = tempList.toArray(String[]::new);

            modifiedOutput.add(tempString);
            tempList.clear();
        }

        return modifiedOutput;
    }

    private static List<Integer> getListOfWhiteSpaces(List<String[]> list) {
        List<Integer> numberOfSpaces = new LinkedList<>();
        var count = 0;

        while (list.get(0).length > count) {
            var length = 100;

            for (String[] strings : list) {
                for (var j = 0; j < strings.length; j++) {
                    var isWord = strings[j].endsWith(ROW) || strings[j].endsWith(ROW_END);
                    if (j == count && isWord) {
                        length = Math.min(countWhiteSpaces(strings[j]), length);
                        break;
                    }
                }
            }
            numberOfSpaces.add(length);
            count++;
        }
        return numberOfSpaces;
    }

    private static int countWhiteSpaces(String str) {
        var count = 0;
        for (var i = 0; i < str.length(); i++) {
            if (Character.isSpaceChar(str.charAt(i))) {
                count++;
            }
        }
        if (str.endsWith(ROW_END)) {
            count--;
        }
        return count;
    }
}

