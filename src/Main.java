import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class Main {
    static final String MARIA = "Maria";
    static final String FLORIAN = "Florian";
    static final String DAVID = "David";
    static final String ANNE = "Anne";
    static final String JAKOB = "Jakob";
    static final String TANJA = "Tanja";
    static final String ADAM = "Adam";
    static final String ANKE = "Anke";
    static final String GUSTAV = "Gustav";
    static final String KLAUS = "Klaus";
    static final String SUSANNE = "Susanne";
    static final String BERND = "Bernd";
    static final String DORIS = "Doris";
    static final String EVA = "Eva";
    static final String KAI = "Kai";
    static final String BIRGIT = "Birgit";
    static final String RALF = "Ralf";
    static final String NINA = "Nina";
    static final String TIM = "Tim";
    static final String SOPHIE = "Sophie";
    static final String MIA = "Mia";
    static final String PIA = "Pia";
    static final String MORITZ = "Moritz";
    static final String FINN = "Finn";
    static final String MAX = "Max";
    static final String SARAH = "Sarah";
    static final String LEONIE = "Leonie";
    static final String FABIAN = "Fabian";
    ////////////////////
    static final String LEA = "Lea";
    static final String BIBI = "Bibi";
    static final String BOBO = "Bobo";
    static final String TUTI = "Tuti";
    static final String BOB = "Bob";
    static final String BERG = "Berg";
    static final String MARTA = "Marta";
    static final String TOMAS = "Tomas";
    ////////////////////
    static final String STANLEY = "Stanley";
    static final String SALLY = "Sally";
    static final String OLGA = "Olga";
    static final String EVGENY = "Evgeny";
    static final String IRINA = "Irina";
    static final String WALDEMAR = "Waldemar";
    static final String ALEXANDER = "Alexander";
    static final String ROSA = "Rosa";
    static final String ANDREY = "Andrey";
    static final String VIKTOR = "Viktor";
    static final String VALENTINA = "Valentina";
    static final String LILIA = "Lilia";

    static final String[] s_Abstammung = {
            DAVID, MARIA, FLORIAN,
            TANJA, ANNE, JAKOB,
            ADAM, TANJA, DAVID,
            KLAUS, ANKE, GUSTAV,
            DORIS, SUSANNE, BERND,
            KAI, EVA, ADAM,
            EVA, DORIS, KLAUS,
            BIRGIT, MARIA, FLORIAN,
            RALF, MARIA, FLORIAN,
            NINA, ANNE, JAKOB,
            TIM, ANNE, JAKOB,
            SOPHIE, NINA, RALF,
            MIA, ANKE, BERND,
            PIA, ANKE, BERND,
            MORITZ, SUSANNE, GUSTAV,
            FINN, SUSANNE, GUSTAV,
            MAX, PIA, FINN,
            SARAH, SOPHIE, MAX,
            GUSTAV, LEONIE, FABIAN,

            LEONIE, LEA, BOB,
            FABIAN, MARTA, TOMAS,
            LEA, BIBI, BOBO,
            BOB, TUTI, BERG,

            BERG, OLGA, EVGENY,
            OLGA, IRINA, WALDEMAR,
            EVGENY, VALENTINA, VIKTOR,
            VALENTINA, LILIA, ANDREY,
            VIKTOR, ROSA, ALEXANDER
    };

    public static class TreeInfo {
        public int maxDepth;
        public int maxEntryLength;
        public int maxLineLength;
        public ArrayList<Integer> maxEntryLengthPerDepth;

        public TreeInfo(int maxDepth, int maxEntryLength, int maxLineLength, ArrayList<Integer> maxEntryLengthPerDepth) {
            this.maxDepth = maxDepth;
            this.maxEntryLength = maxEntryLength; // TODO: Don't need this
            this.maxLineLength = maxLineLength;
            this.maxEntryLengthPerDepth = maxEntryLengthPerDepth;
        }
    }

    public static class TreeNode {
        public final String name;
        public TreeNode mother;
        public TreeNode father;

        public TreeNode(String name, TreeNode mother, TreeNode father) {
            this.name = name;
            this.mother = mother;
            this.father = father;
        }
    }

    public static class Tree {
        private final HashMap<String, TreeNode> Nodes;

        public Tree(String[] entries) {
            Nodes = new HashMap<>();

            if (entries.length % 3 != 0) {
                System.err.println("MALFORMED INPUT DATA");
                return;
            }

            for (int i = 0; i < entries.length; i += 3) {
                String child = entries[i + 0];
                String mother = entries[i + 1];
                String father = entries[i + 2];
                add(child, mother, father);
            }
        }

        public void add(String name, String mother, String father) {
            if (get(mother) == null) {
                Nodes.put(mother, new TreeNode(mother, null, null));
            }

            if (get(father) == null) {
                Nodes.put(father, new TreeNode(father, null, null));
            }

            TreeNode entry = get(name);
            if (entry != null) {
                entry.mother = get(mother);
                entry.father = get(father);
            } else {
                Nodes.put(name, new TreeNode(name, get(mother), get(father)));
            }
        }

        public TreeNode get(String name) {
            return Nodes.get(name);
        }

        private TreeInfo extractInfo(TreeNode node, TreeInfo currentTraceInfo, boolean initial) {
            if (node == null) {
                return currentTraceInfo;
            }

            int entryLength = node.name.length();
            int nextDepth = currentTraceInfo.maxDepth + 1;
            int maxEntryLength = Math.max(currentTraceInfo.maxEntryLength, entryLength);
            int maxLineLength = currentTraceInfo.maxLineLength;

            if (currentTraceInfo.maxEntryLengthPerDepth.size() < nextDepth) {
                currentTraceInfo.maxEntryLengthPerDepth.add(0);
            }
            currentTraceInfo.maxEntryLengthPerDepth.set(currentTraceInfo.maxDepth, Math.max(currentTraceInfo.maxEntryLengthPerDepth.get(currentTraceInfo.maxDepth), entryLength));

            int motherMaxLine = node.mother == null ? 0 : node.mother.name.length();
            int fatherMaxLine = node.father == null ? 0 : node.father.name.length();
            int maxLine = maxLineLength + Math.max(motherMaxLine, fatherMaxLine);

            TreeInfo motherInfo = extractInfo(node.mother, new TreeInfo(nextDepth, maxEntryLength, maxLine, currentTraceInfo.maxEntryLengthPerDepth), false);
            TreeInfo fatherInfo = extractInfo(node.father, new TreeInfo(nextDepth, maxEntryLength, maxLine, currentTraceInfo.maxEntryLengthPerDepth), false);

            return new TreeInfo(
                    Math.max(motherInfo.maxDepth,       fatherInfo.maxDepth),
                    Math.max(motherInfo.maxEntryLength, fatherInfo.maxEntryLength),
                    Math.max(motherInfo.maxLineLength,  fatherInfo.maxLineLength) + (initial ? entryLength : 0),
                    currentTraceInfo.maxEntryLengthPerDepth
            );
        }

        private void printNodes(TreeNode node, TreeInfo info,
                                int currMaxDepth, LinkedList<TreeNode> toPrint,
                                ArrayList<Boolean> slots, LinkedList<TreeNode> visited)
        {
            if (node != null) {
                if (!toPrint.contains(node)) {
                    toPrint.addFirst(node);
                }

                if (node.mother != null) {
                    printNodes(node.mother, info, currMaxDepth + 1, toPrint, slots, visited);
                }

                if (toPrint.size() > 0) {
                    StringBuilder sb = new StringBuilder();

                    // Prepend
                    int offset = info.maxDepth - 1 - currMaxDepth;

                    for (int i = 0; i < offset; i++) {
                        int maxEntry = info.maxEntryLengthPerDepth.get(currMaxDepth + i + 1);
                        sb.append(" ".repeat(Math.max(0, maxEntry)));
                        sb.append("     ");
                    }

                    for (int i = 0; i < toPrint.size(); i++) {
                        TreeNode n = toPrint.get(i);
                        // Build up line
                        sb.append(n.name);

                        // append padding
                        int entryDepth = currMaxDepth - i;
                        int maxEntry = info.maxEntryLengthPerDepth.get(entryDepth);
                        sb.append(" ".repeat(Math.max(0, maxEntry - n.name.length())));

                        int namesInFront = toPrint.size() - i - 1;
                        if (namesInFront >= 1 ) {
                            sb.append(" -+- ");
                            slots.set(entryDepth, true);
                        } else if (namesInFront == 0 && entryDepth != 0) {
                            sb.append(" -+");
                            slots.set(entryDepth, false);
                        }

                        // Draw '!'
                        if (entryDepth > 0 && i == toPrint.size() - 1) {
                            for (int si = entryDepth-1; si >= 0; si--) {
                                boolean b = slots.get(si);
                                int maxEntryDepth = info.maxEntryLengthPerDepth.get(si);
                                sb.append("  ");
                                sb.append(" ".repeat(maxEntryDepth));
                                sb.append(b ? "  !" : "   ");
                            }
                        }
                    }

                    System.out.println(sb.toString());
                    toPrint.clear();
                }

                if (node.father != null) {
                    printNodes(node.father, info, currMaxDepth + 1, toPrint, slots, visited);

                    if (!visited.contains(node.father)
                            && !visited.contains(node.mother)
                            && !visited.contains(node))
                    {
                        visited.add(node);
                        visited.add(node.father);
                        visited.add(node.mother);

                        boolean hasSlots = false;
                        for (boolean b : slots) {
                            hasSlots |= b;
                        }

                        if (hasSlots) {
                            StringBuilder sb = new StringBuilder();
                            for (int si = info.maxDepth-1; si >= 0; si--) {
                                boolean b = slots.get(si);
                                int maxEntryDepth = info.maxEntryLengthPerDepth.get(si);
                                sb.append(" ".repeat(maxEntryDepth));
                                sb.append(b ? "  !  " : "     ");
                            }
                            System.out.println(sb);
                        }
                    }
                }
            }
        }

        public void print(TreeNode node) {
            TreeInfo info = extractInfo(node, new TreeInfo(0, 0, 0, new ArrayList<>()), true);

            ArrayList<Boolean> slots = new ArrayList<>();
            for (int i = 0; i < info.maxDepth; i++) { slots.add(false); }

            printNodes(node, info, 0, new LinkedList<>(), slots, new LinkedList<>());
        }
    }

    public void start() {
        Tree familyTree = new Tree(s_Abstammung);
        familyTree.print(familyTree.get(KAI));
    }

    public static void main(String[] args) {
        long start = System.nanoTime();

        Main main = new Main();
        main.start();

        long end = System.nanoTime();
        long elapsed = end - start;
        double seconds =  elapsed / 1_000_000_000.0;
        System.out.printf("EXEC TIME : %f seconds\n", seconds);
    }
}
